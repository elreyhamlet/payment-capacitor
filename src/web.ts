import { WebPlugin } from '@capacitor/core';
import { ParametrosPayment, PaymentPlugin, ResultadoPayment } from './definitions';

export class PaymentWeb extends WebPlugin implements PaymentPlugin {
  constructor() {
    super({
      name: 'Payment',
      platforms: ['web'],
    });
  }

  async iniciarSdkPayment(options: ParametrosPayment): Promise<ResultadoPayment> {
    return options.resultado;
  }

  

}

const Payment = new PaymentWeb();

export { Payment };
import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(Payment);
