declare module '@capacitor/core' {
  interface PluginRegistry {
    Payment: PaymentPlugin;
  }
}

export interface PaymentPlugin {
  iniciarSdkPayment(options: ParametrosPayment): Promise<ResultadoPayment>;
}

export interface ParametrosPayment {
  publicKey: string,
  apiServerNamePayment: string,
  apiServerName: string,
  authUser: string,
  authPassword: string,
  moneda: string,
  monto: number,
  resultado: ResultadoPayment
}
export interface ResultadoPayment {
  estado: boolean,
  mensaje: string
  payload: any
}


