
package com.wydnex.payment;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.getcapacitor.JSObject;
import com.getcapacitor.PluginCall;
import com.lyra.sdk.Lyra;
import com.lyra.sdk.callback.LyraHandler;
import com.lyra.sdk.callback.LyraResponse;
import com.lyra.sdk.exception.LyraException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MetodosGlobales {

    private Context context;
    private PluginCall pluginCall;
    private FragmentManager fragmentManager;
    private String apiServerName;
    private String apiServerNamePayment;
    private String publicKey;
    private String authUser;
    private String authPassword;
    private Double monto;
    private String moneda;


    public MetodosGlobales(
            Context context,
            PluginCall pluginCall,
            FragmentManager fragmentManager,
            String apiServerName,
            String apiServerNamePayment,
            String publicKey,
            String authUser,
            String authPassword,
            Double monto,
            String moneda) {
        this.context = context;
        this.pluginCall = pluginCall;
        this.fragmentManager = fragmentManager;
        this.apiServerName = apiServerName;
        this.apiServerNamePayment = apiServerNamePayment;
        this.publicKey = publicKey;
        this.authUser = authUser;
        this.authPassword = authPassword;
        this.monto = monto;
        this.moneda = moneda;
    }

    public JSObject configurarSdk() {
        JSObject payload = new JSObject();
        HashMap options = new HashMap();
        try {
            options.put(Lyra.OPTION_API_SERVER_NAME, apiServerNamePayment);
            Lyra.INSTANCE.initialize(context.getApplicationContext(), publicKey, options);
            payload.put("estado", true);
            payload.put("mensaje", "OK!!!");
            return payload;
        } catch (Exception e) {
            payload.put("estado", false);
            payload.put("mensaje", "El Sistema de pago no se inicializó, inténtalo otra vez");
            payload.put("payload", e.toString());
            return payload;
        }
    }

    public void iniciarPago() {
        JSObject payload = new JSObject();
        JSObject respone = new JSObject();
        RequestQueue queue = Volley.newRequestQueue(context);
        int token = Lyra.INSTANCE.getFormTokenVersion();
        String valoFinal = String.format("%03d", (int) (monto * 100));
        respone.put("formTokenVersion", token);
        respone.put("amount", valoFinal);
        respone.put("currency", moneda);
        queue.add(new JsonObjectRequest(Request.Method.POST, apiServerName + "/createPayment", respone, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                createPayment(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                payload.put("estado", false);
                payload.put("mensaje", "No se puedo inicializar, inténtelo de nuevo");
                payload.put("payload", error.toString());
                pluginCall.resolve(payload);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = authUser + ":" + authPassword;
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        }).setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public void createPayment(final JSONObject createResponse) {
        JSObject payload = new JSObject();
        Lyra.INSTANCE.process(fragmentManager, createResponse.toString(), new LyraHandler() {
            @Override
            public void onSuccess(LyraResponse lyraResponse) {
                verificar(lyraResponse);
            }

            @Override
            public void onError(LyraException e, LyraResponse lyraResponse) {
                payload.put("estado", false);
                payload.put("mensaje", e.getErrorMessage());
                payload.put("payload", e.toString());
                pluginCall.resolve(payload);
            }

        });
    }

    public void verificar(LyraResponse response) {
        RequestQueue queue = Volley.newRequestQueue(context);
        JSObject payload = new JSObject();
        queue.add(new JsonObjectRequest(Request.Method.POST, apiServerName + "/verifyResult",
                response, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject res) {
                payload.put("estado", true);
                payload.put("mensaje", "Ok");
                payload.put("payload", response);
                pluginCall.resolve(payload);

            }
        }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                payload.put("estado", false);
                payload.put("mensaje", error.getMessage());
                payload.put("payload",error.toString());
                pluginCall.resolve(payload);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = authUser + ":" + authPassword;
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }
        }).setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


}
